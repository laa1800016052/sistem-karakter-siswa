import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomewaliPage } from './homewali.page';

const routes: Routes = [
  {
    path: '',
    component: HomewaliPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomewaliPageRoutingModule {}
