import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomewaliPage } from './homewali.page';

describe('HomewaliPage', () => {
  let component: HomewaliPage;
  let fixture: ComponentFixture<HomewaliPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomewaliPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomewaliPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
