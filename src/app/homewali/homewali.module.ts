import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomewaliPageRoutingModule } from './homewali-routing.module';

import { HomewaliPage } from './homewali.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomewaliPageRoutingModule
  ],
  declarations: [HomewaliPage]
})
export class HomewaliPageModule {}
