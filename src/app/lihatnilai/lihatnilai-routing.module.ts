import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LihatnilaiPage } from './lihatnilai.page';

const routes: Routes = [
  {
    path: '',
    component: LihatnilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LihatnilaiPageRoutingModule {}
