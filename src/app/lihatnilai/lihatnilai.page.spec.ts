import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LihatnilaiPage } from './lihatnilai.page';

describe('LihatnilaiPage', () => {
  let component: LihatnilaiPage;
  let fixture: ComponentFixture<LihatnilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LihatnilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LihatnilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
