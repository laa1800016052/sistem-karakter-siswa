import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PenangananPage } from './penanganan.page';

describe('PenangananPage', () => {
  let component: PenangananPage;
  let fixture: ComponentFixture<PenangananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenangananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PenangananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
