import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PenangananPage } from './penanganan.page';

const routes: Routes = [
  {
    path: '',
    component: PenangananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PenangananPageRoutingModule {}
