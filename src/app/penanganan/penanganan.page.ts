import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { FormPenangananPage } from '../form-penanganan/form-penanganan.page';
import { HomePage } from '../home/home.page';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { HalPenangananPage } from '../hal-penanganan/hal-penanganan.page';

@Component({
  selector: 'app-penanganan',
  templateUrl: './penanganan.page.html',
  styleUrls: ['./penanganan.page.scss'],
})
export class PenangananPage implements OnInit {

  dataform: any = [];

  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public modalController: ModalController,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.getForm();
  }
  selectedSegments: any='penanganan';

  loading:boolean;
  
  getForm(){
    this.dataform = [];
    this.db.collection('form').valueChanges({idField: 'id'}).subscribe(res => {
      this.dataform = res;
    })
  }
  async home() {
    const modal = await this.modalController.create({
      component: HomePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async formpenanganan(){
    const modal = await this.modalController.create({
      component: FormPenangananPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async editdata(id: any){
    const modal = await this.modalController.create({
      component: HalPenangananPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  update(nama, nim, kelas, keterangan, kategori, pemberitahuan){
    console.log(nama, nim, kelas, keterangan, kategori, pemberitahuan);
      this.navCtrl.navigateForward(['/hal-penanganan'])
    }
  
  hapus(id: any) {
    var r = confirm("Anda yakin ingin menghapus data ini secara permanen ?");
    if (r == true) {
      this.db.collection('form').doc(id).delete();
      alert('Pengguna baru berhasil dihapus');
    } else {
      return;
    }
  }
  
}
