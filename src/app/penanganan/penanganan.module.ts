import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PenangananPageRoutingModule } from './penanganan-routing.module';

import { PenangananPage } from './penanganan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PenangananPageRoutingModule
  ],
  declarations: [PenangananPage]
})
export class PenangananPageModule {}
