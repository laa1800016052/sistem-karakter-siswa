import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailNilaiPage } from './detail-nilai.page';

describe('DetailNilaiPage', () => {
  let component: DetailNilaiPage;
  let fixture: ComponentFixture<DetailNilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailNilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailNilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
