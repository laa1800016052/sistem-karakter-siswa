import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditNilaiPage } from './edit-nilai.page';

describe('EditNilaiPage', () => {
  let component: EditNilaiPage;
  let fixture: ComponentFixture<EditNilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditNilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
