import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditNilaiPage } from './edit-nilai.page';

const routes: Routes = [
  {
    path: '',
    component: EditNilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditNilaiPageRoutingModule {}
