import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HasilKetercapaianPageRoutingModule } from './hasil-ketercapaian-routing.module';

import { HasilKetercapaianPage } from './hasil-ketercapaian.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HasilKetercapaianPageRoutingModule
  ],
  declarations: [HasilKetercapaianPage]
})
export class HasilKetercapaianPageModule {}
