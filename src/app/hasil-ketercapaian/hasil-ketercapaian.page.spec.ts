import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HasilKetercapaianPage } from './hasil-ketercapaian.page';

describe('HasilKetercapaianPage', () => {
  let component: HasilKetercapaianPage;
  let fixture: ComponentFixture<HasilKetercapaianPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HasilKetercapaianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HasilKetercapaianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
