import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HasilKetercapaianPage } from './hasil-ketercapaian.page';

const routes: Routes = [
  {
    path: '',
    component: HasilKetercapaianPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HasilKetercapaianPageRoutingModule {}
