import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InputnilaiPageRoutingModule } from './inputnilai-routing.module';

import { InputnilaiPage } from './inputnilai.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InputnilaiPageRoutingModule
  ],
  declarations: [InputnilaiPage]
})
export class InputnilaiPageModule {}
