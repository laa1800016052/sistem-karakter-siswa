import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InputnilaiPage } from './inputnilai.page';

describe('InputnilaiPage', () => {
  let component: InputnilaiPage;
  let fixture: ComponentFixture<InputnilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputnilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InputnilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
