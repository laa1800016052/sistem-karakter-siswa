import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InputnilaiPage } from './inputnilai.page';

const routes: Routes = [
  {
    path: '',
    component: InputnilaiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InputnilaiPageRoutingModule {}
