import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

import { DetailrekapPage} from '../detailrekap/detailrekap.page';

@Component({
  selector: 'app-inputnilai',
  templateUrl: './inputnilai.page.html',
  styleUrls: ['./inputnilai.page.scss'],
})
export class InputnilaiPage implements OnInit {
  rekap: any[];
  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    fetch('./assets/rekap.json').then(res => res.json())
    .then(json => {
      this.rekap = json;
    });
}
selectedSegments: any='rekapdata';


async lihat() {
  const modal = await this.modalController.create({
    component: DetailrekapPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
}
