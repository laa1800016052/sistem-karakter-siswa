import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomemapelPageRoutingModule } from './homemapel-routing.module';

import { HomemapelPage } from './homemapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomemapelPageRoutingModule
  ],
  declarations: [HomemapelPage]
})
export class HomemapelPageModule {}
