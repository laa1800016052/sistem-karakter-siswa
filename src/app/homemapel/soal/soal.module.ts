import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SoalPageRoutingModule } from './soal-routing.module';

import { SoalPage } from './soal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SoalPageRoutingModule
  ],
  declarations: [SoalPage]
})
export class SoalPageModule {}
