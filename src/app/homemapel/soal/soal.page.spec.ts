import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SoalPage } from './soal.page';

describe('SoalPage', () => {
  let component: SoalPage;
  let fixture: ComponentFixture<SoalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SoalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
