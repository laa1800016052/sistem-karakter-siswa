import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomemapelPage } from './homemapel.page';

const routes: Routes = [
  {
    path: '',
    component: HomemapelPage
  },
  {
    path: 'soal',
    loadChildren: () => import('./soal/soal.module').then( m => m.SoalPageModule)
  },  {
    path: 'jadwal-penilaian',
    loadChildren: () => import('./jadwal-penilaian/jadwal-penilaian.module').then( m => m.JadwalPenilaianPageModule)
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomemapelPageRoutingModule {}
