import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HomemapelPage } from '../homemapel.page';

@Component({
  selector: 'app-jadwal-penilaian',
  templateUrl: './jadwal-penilaian.page.html',
  styleUrls: ['./jadwal-penilaian.page.scss'],
})
export class JadwalPenilaianPage implements OnInit {

  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async home() {
    const modal = await this.modalController.create({
      component: HomemapelPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
