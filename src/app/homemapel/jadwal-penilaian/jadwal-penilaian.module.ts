import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JadwalPenilaianPageRoutingModule } from './jadwal-penilaian-routing.module';

import { JadwalPenilaianPage } from './jadwal-penilaian.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JadwalPenilaianPageRoutingModule
  ],
  declarations: [JadwalPenilaianPage]
})
export class JadwalPenilaianPageModule {}
