import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeortuPage } from './homeortu.page';

const routes: Routes = [
  {
    path: '',
    component: HomeortuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeortuPageRoutingModule {}
