import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeortuPage } from './homeortu.page';

describe('HomeortuPage', () => {
  let component: HomeortuPage;
  let fixture: ComponentFixture<HomeortuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeortuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeortuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
