import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoPage } from './lo.page';

describe('LoPage', () => {
  let component: LoPage;
  let fixture: ComponentFixture<LoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
