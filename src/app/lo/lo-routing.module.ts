import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoPage } from './lo.page';

const routes: Routes = [
  {
    path: '',
    component: LoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoPageRoutingModule {}
