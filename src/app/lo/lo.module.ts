import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoPageRoutingModule } from './lo-routing.module';

import { LoPage } from './lo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoPageRoutingModule
  ],
  declarations: [LoPage]
})
export class LoPageModule {}
