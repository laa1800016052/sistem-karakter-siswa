import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lb',
  templateUrl: './lb.page.html',
  styleUrls: ['./lb.page.scss'],
})
export class LbPage implements OnInit {

  user: any = {};
  constructor(
    private auth: AngularFireAuth,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  loading: boolean;
  login(){
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.router.navigate(['/home']);
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat login');
    });
  }
}
