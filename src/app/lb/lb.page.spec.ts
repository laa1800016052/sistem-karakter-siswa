import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LbPage } from './lb.page';

describe('LbPage', () => {
  let component: LbPage;
  let fixture: ComponentFixture<LbPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LbPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
