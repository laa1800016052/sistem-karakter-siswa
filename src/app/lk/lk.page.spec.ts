import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LkPage } from './lk.page';

describe('LkPage', () => {
  let component: LkPage;
  let fixture: ComponentFixture<LkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
