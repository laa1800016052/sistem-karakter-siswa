import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LkPage } from './lk.page';

const routes: Routes = [
  {
    path: '',
    component: LkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LkPageRoutingModule {}
