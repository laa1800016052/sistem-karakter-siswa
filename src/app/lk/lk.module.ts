import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LkPageRoutingModule } from './lk-routing.module';

import { LkPage } from './lk.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LkPageRoutingModule
  ],
  declarations: [LkPage]
})
export class LkPageModule {}
