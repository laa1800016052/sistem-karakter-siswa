import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-lihatjadwal',
  templateUrl: './lihatjadwal.page.html',
  styleUrls: ['./lihatjadwal.page.scss'],
})
export class LihatjadwalPage implements OnInit {

  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async home() {
    const modal = await this.modalController.create({
      component: HomePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
