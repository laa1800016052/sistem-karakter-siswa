import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { HomesiswaPage } from '../homesiswa/homesiswa.page';

@Component({
  selector: 'app-lihatsoal',
  templateUrl: './lihatsoal.page.html',
  styleUrls: ['./lihatsoal.page.scss'],
})
export class LihatsoalPage implements OnInit {
  datasoal: any = [];

  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.getSoal();
  }

  selectedSegments: any='soal';

  loading:boolean;
  
  getSoal(){
    this.db.collection('data_soal').valueChanges({idField: 'id'}).subscribe(res=>{
    this.datasoal = res;
    console.log(res);
    })
  }
  async home() {
    const modal = await this.modalController.create({
      component: HomesiswaPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
