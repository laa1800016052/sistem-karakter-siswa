import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LihatsoalPageRoutingModule } from './lihatsoal-routing.module';

import { LihatsoalPage } from './lihatsoal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LihatsoalPageRoutingModule
  ],
  declarations: [LihatsoalPage]
})
export class LihatsoalPageModule {}
