import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomekepsekPageRoutingModule } from './homekepsek-routing.module';

import { HomekepsekPage } from './homekepsek.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomekepsekPageRoutingModule
  ],
  declarations: [HomekepsekPage]
})
export class HomekepsekPageModule {}
