import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { JadwalMapelPage } from '../jadwal-mapel/jadwal-mapel.page';
import { LihatjadwalPage } from '../lihatjadwal/lihatjadwal.page';
import { LihatsoalPage } from '../lihatsoal/lihatsoal.page';
import { ProfilPage } from '../profil/profil.page';
import { RekapdataPage } from '../rekapdata/rekapdata.page';
@Component({
  selector: 'app-homesiswa',
  templateUrl: './homesiswa.page.html',
  styleUrls: ['./homesiswa.page.scss'],
})
export class HomesiswaPage implements OnInit {
  constructor(
    public router: Router,
    public modalController: ModalController
  ) {}
  ngOnInit(){
  }
  selectedSegments: any='home';

async profil() {
  const modal = await this.modalController.create({
    component: ProfilPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}

async lihatsoal() {
  const modal = await this.modalController.create({
    component: LihatsoalPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
}
