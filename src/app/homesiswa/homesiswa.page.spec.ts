import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomesiswaPage } from './homesiswa.page';

describe('HomesiswaPage', () => {
  let component: HomesiswaPage;
  let fixture: ComponentFixture<HomesiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomesiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomesiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
