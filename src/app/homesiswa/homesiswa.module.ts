import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomesiswaPageRoutingModule } from './homesiswa-routing.module';

import { HomesiswaPage } from './homesiswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomesiswaPageRoutingModule
  ],
  declarations: [HomesiswaPage]
})
export class HomesiswaPageModule {}
