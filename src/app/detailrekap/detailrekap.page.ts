import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { RekapdataPage } from '../rekapdata/rekapdata.page';
@Component({
  selector: 'app-detailrekap',
  templateUrl: './detailrekap.page.html',
  styleUrls: ['./detailrekap.page.scss'],
})
export class DetailrekapPage implements OnInit {

  rekap: any[];
  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    fetch('./assets/rekap.json').then(res => res.json())
    .then(json => {
      this.rekap = json;
    });
}
selectedSegments: any='DETAILREKAP';

}
