import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailrekapPageRoutingModule } from './detailrekap-routing.module';

import { DetailrekapPage } from './detailrekap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailrekapPageRoutingModule
  ],
  declarations: [DetailrekapPage]
})
export class DetailrekapPageModule {}
