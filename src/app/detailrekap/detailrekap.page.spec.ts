import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailrekapPage } from './detailrekap.page';

describe('DetailrekapPage', () => {
  let component: DetailrekapPage;
  let fixture: ComponentFixture<DetailrekapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailrekapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailrekapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
