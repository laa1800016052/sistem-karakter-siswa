import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailrekapPage } from './detailrekap.page';

const routes: Routes = [
  {
    path: '',
    component: DetailrekapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailrekapPageRoutingModule {}
