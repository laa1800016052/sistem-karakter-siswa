import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-lupa-password',
  templateUrl: './lupa-password.page.html',
  styleUrls: ['./lupa-password.page.scss'],
})
export class LupaPasswordPage implements OnInit {

  email: string = "";

  constructor(
    private auth: AngularFireAuth,
    public router: Router,
    public alert:AlertController,
  ) { }

  ngOnInit() {
  }

  loading: boolean;
  forget(){
    this.loading = true;
    this.auth.sendPasswordResetEmail(this.email).then(res=>{
      alert('Permintaan berhasil diproses", "Link telah dikirim ke email anda');
      this.router.navigate(['/login']);
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat reset');
    });
  }

}