import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { PenangananPage } from '../penanganan/penanganan.page';
import { ProfilPage } from '../profil/profil.page';
import { RekapdataPage } from '../rekapdata/rekapdata.page';
import { LihatjadwalPage } from '../lihatjadwal/lihatjadwal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor(
    public router: Router,
    public modalController: ModalController
  ) {}
  ngOnInit(){
  }
  selectedSegments: any='home';

async profil() {
  const modal = await this.modalController.create({
    component: ProfilPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
async rekapdata() {
  const modal = await this.modalController.create({
    component: RekapdataPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
async jadwal() {
  const modal = await this.modalController.create({
    component: LihatjadwalPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
async penanganan() {
  const modal = await this.modalController.create({
    component: PenangananPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
  }
}
