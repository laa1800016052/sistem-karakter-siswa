import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapdataPage } from './rekapdata.page';

describe('RekapdataPage', () => {
  let component: RekapdataPage;
  let fixture: ComponentFixture<RekapdataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapdataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapdataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
