import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { DetailPenilaianPage } from '../detail-penilaian/detail-penilaian.page';
import { HasilKetercapaianPage } from '../hasil-ketercapaian/hasil-ketercapaian.page';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-rekapdata',
  templateUrl: './rekapdata.page.html',
  styleUrls: ['./rekapdata.page.scss'],
})
export class RekapdataPage implements OnInit {

  students: any[];
  
  constructor(
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    fetch('./assets/student.json').then(res => res.json())
      .then(json => {
        this.students = json;
      });
  }
  selectedSegments: any='rekapdata';

async home() {
  const modal = await this.modalController.create({
    component: HomePage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
async lihathasil() {
  const modal = await this.modalController.create({
    component: DetailPenilaianPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}

async hasilketercapaian(){
  const modal = await this.modalController.create({
    component: HasilKetercapaianPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
}
