import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { RekapDataPageRoutingModule } from './rekap-data-routing.module';

import { RekapDataPage } from './rekap-data.page';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    IonicModule,
    RekapDataPageRoutingModule
  ],
  declarations: [RekapDataPage]
})
export class RekapDataPageModule {}
