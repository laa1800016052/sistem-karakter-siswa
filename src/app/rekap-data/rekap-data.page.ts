import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { HomemapelPage } from '../homemapel/homemapel.page';

@Component({
  selector: 'app-rekap-data',
  templateUrl: './rekap-data.page.html',
  styleUrls: ['./rekap-data.page.scss'],
})
export class RekapDataPage implements OnInit {

  datasiswa: any = [];

  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.getDatasiswa();
  }

  loading:boolean;

  getDatasiswa(){
    this.db.collection('data_siswa').valueChanges({idField: 'id'}).subscribe(res=>{
      this.datasiswa = res;
    });
  }

  async homemapel() {
    const modal = await this.modalController.create({
      component: HomemapelPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
