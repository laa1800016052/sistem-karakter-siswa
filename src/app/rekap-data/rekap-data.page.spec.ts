import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapDataPage } from './rekap-data.page';

describe('RekapDataPage', () => {
  let component: RekapDataPage;
  let fixture: ComponentFixture<RekapDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
