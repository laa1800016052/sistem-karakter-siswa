import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

interface User {
  email: string;
  password: string;
  nama:string;
  nip:string;
  alamat:string;
  hp:string
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: any = {};
  userData: User;

  constructor(
    public toastController: ToastController,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public router: Router
  ) { }

  ngOnInit() {
  }
  
  async pesanKesalahan(){
    const toast = await this.toastController.create({
      message: 'Tidak dapat melakukan registrasi',
      duration: 2000,
      position: 'middle'
    });
    toast.present();
  }

  loading: boolean;
  registrasi(){
    this.loading = true;
    this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.loading = false;
      alert('selamat anda berhasil registrasi')
      this.writeUser(res.user.email);
    }, err=>{
      this.pesanKesalahan();
      this.loading = false;
    })
  }

  writeUser(email){
    this.userData = {
      email: this.user.email,
      password: this.user.password,
      nama: this.user.nama,
      nip: this.user.nip,
      alamat: this.user.alamat,
      hp: this.user.hp
    };
    this.db.collection('users').doc(email).set(this.userData).then(res=>{
      this.loading =  false;
      this.router.navigate(['/login']);
    },err=>{
      console.log(err);
      this.pesanKesalahan();
      this.loading = false;
    })
  }

}
