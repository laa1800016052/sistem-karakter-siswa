import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-lw',
  templateUrl: './lw.page.html',
  styleUrls: ['./lw.page.scss'],
})
export class LwPage implements OnInit {

 
  user: any = {};

  constructor(
    private auth: AngularFireAuth,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  loading: boolean;
  login(){
    this.loading = true;
    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
      this.router.navigate(['/homewali']);
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat login');
    });
  }
}
