import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LwPage } from './lw.page';

const routes: Routes = [
  {
    path: '',
    component: LwPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LwPageRoutingModule {}
