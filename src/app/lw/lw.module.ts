import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LwPageRoutingModule } from './lw-routing.module';

import { LwPage } from './lw.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LwPageRoutingModule
  ],
  declarations: [LwPage]
})
export class LwPageModule {}
