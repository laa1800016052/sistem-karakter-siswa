import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LmPage } from './lm.page';

const routes: Routes = [
  {
    path: '',
    component: LmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LmPageRoutingModule {}
