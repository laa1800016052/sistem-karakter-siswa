import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LmPage } from './lm.page';

describe('LmPage', () => {
  let component: LmPage;
  let fixture: ComponentFixture<LmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
