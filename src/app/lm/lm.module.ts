import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LmPageRoutingModule } from './lm-routing.module';

import { LmPage } from './lm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LmPageRoutingModule
  ],
  declarations: [LmPage]
})
export class LmPageModule {}
