import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LsPage } from './ls.page';

describe('LsPage', () => {
  let component: LsPage;
  let fixture: ComponentFixture<LsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
