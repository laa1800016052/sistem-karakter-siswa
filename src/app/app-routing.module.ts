import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'pilihan',
    pathMatch: 'full'
  },
  {
    path: 'rekapdata',
    loadChildren: () => import('./rekapdata/rekapdata.module').then( m => m.RekapdataPageModule)
  },
  {
    path: 'penanganan',
    loadChildren: () => import('./penanganan/penanganan.module').then( m => m.PenangananPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'form-penanganan',
    loadChildren: () => import('./form-penanganan/form-penanganan.module').then( m => m.FormPenangananPageModule)
  },
  {
    path: 'detail-penilaian',
    loadChildren: () => import('./detail-penilaian/detail-penilaian.module').then( m => m.DetailPenilaianPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'lupa-password',
    loadChildren: () => import('./lupa-password/lupa-password.module').then( m => m.LupaPasswordPageModule)
  },
  {
    path: 'homesiswa',
    loadChildren: () => import('./homesiswa/homesiswa.module').then( m => m.HomesiswaPageModule)
  },
  {
    path: 'homeortu',
    loadChildren: () => import('./homeortu/homeortu.module').then( m => m.HomeortuPageModule)
  },
  {
    path: 'homekepsek',
    loadChildren: () => import('./homekepsek/homekepsek.module').then( m => m.HomekepsekPageModule)
  },
  {
    path: 'homewali',
    loadChildren: () => import('./homewali/homewali.module').then( m => m.HomewaliPageModule)
  },
  {
    path: 'homemapel',
    loadChildren: () => import('./homemapel/homemapel.module').then( m => m.HomemapelPageModule)
  },
  {
    path: 'pilihan',
    loadChildren: () => import('./pilihan/pilihan.module').then( m => m.PilihanPageModule)
  },
  {
    path: 'ls',
    loadChildren: () => import('./ls/ls.module').then( m => m.LsPageModule)
  },
  {
    path: 'lo',
    loadChildren: () => import('./lo/lo.module').then( m => m.LoPageModule)
  },
  {
    path: 'lm',
    loadChildren: () => import('./lm/lm.module').then( m => m.LmPageModule)
  },
  {
    path: 'lw',
    loadChildren: () => import('./lw/lw.module').then( m => m.LwPageModule)
  },
  {
    path: 'lb',
    loadChildren: () => import('./lb/lb.module').then( m => m.LbPageModule)
  },
  {
    path: 'lk',
    loadChildren: () => import('./lk/lk.module').then( m => m.LkPageModule)
  },
  {
    path: 'lihatjadwal',
    loadChildren: () => import('./lihatjadwal/lihatjadwal.module').then( m => m.LihatjadwalPageModule)
  },
  {
    path: 'lihatsoal',
    loadChildren: () => import('./lihatsoal/lihatsoal.module').then( m => m.LihatsoalPageModule)
  },
  {
    path: 'lihatnilai',
    loadChildren: () => import('./lihatnilai/lihatnilai.module').then( m => m.LihatnilaiPageModule)
  },
  {
    path: 'hasil-ketercapaian',
    loadChildren: () => import('./hasil-ketercapaian/hasil-ketercapaian.module').then( m => m.HasilKetercapaianPageModule)
  },
  {
    path: 'tambah',
    loadChildren: () => import('./tambah/tambah.module').then( m => m.TambahPageModule)
  },
  {
    path: 'inputnilai',
    loadChildren: () => import('./inputnilai/inputnilai.module').then( m => m.InputnilaiPageModule)
  },
  {
    path: 'detailrekap',
    loadChildren: () => import('./detailrekap/detailrekap.module').then( m => m.DetailrekapPageModule)
  },
  {
    path: 'hal-penanganan',
    loadChildren: () => import('./hal-penanganan/hal-penanganan.module').then( m => m.HalPenangananPageModule)
  },











];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
