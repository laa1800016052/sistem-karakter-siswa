import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JadwalMapelPageRoutingModule } from './jadwal-mapel-routing.module';

import { JadwalMapelPage } from './jadwal-mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JadwalMapelPageRoutingModule
  ],
  declarations: [JadwalMapelPage]
})
export class JadwalMapelPageModule {}
