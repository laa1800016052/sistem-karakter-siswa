import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JadwalMapelPage } from './jadwal-mapel.page';

describe('JadwalMapelPage', () => {
  let component: JadwalMapelPage;
  let fixture: ComponentFixture<JadwalMapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JadwalMapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JadwalMapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
