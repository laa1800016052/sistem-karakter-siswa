import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JadwalMapelPage } from './jadwal-mapel.page';

const routes: Routes = [
  {
    path: '',
    component: JadwalMapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JadwalMapelPageRoutingModule {}
