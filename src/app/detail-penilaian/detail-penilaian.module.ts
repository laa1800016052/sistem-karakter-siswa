import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPenilaianPageRoutingModule } from './detail-penilaian-routing.module';

import { DetailPenilaianPage } from './detail-penilaian.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPenilaianPageRoutingModule
  ],
  declarations: [DetailPenilaianPage]
})
export class DetailPenilaianPageModule {}
