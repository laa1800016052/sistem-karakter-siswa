import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPenilaianPage } from './detail-penilaian.page';

const routes: Routes = [
  {
    path: '',
    component: DetailPenilaianPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailPenilaianPageRoutingModule {}
