import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditEditProfilPageRoutingModule } from './edit-edit-profil-routing.module';

import { EditEditProfilPage } from './edit-edit-profil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditEditProfilPageRoutingModule
  ],
  declarations: [EditEditProfilPage]
})
export class EditEditProfilPageModule {}
