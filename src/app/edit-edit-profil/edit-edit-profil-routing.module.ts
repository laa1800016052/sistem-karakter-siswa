import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditEditProfilPage } from './edit-edit-profil.page';

const routes: Routes = [
  {
    path: '',
    component: EditEditProfilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditEditProfilPageRoutingModule {}
