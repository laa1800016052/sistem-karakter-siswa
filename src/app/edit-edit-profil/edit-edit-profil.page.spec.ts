import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditEditProfilPage } from './edit-edit-profil.page';

describe('EditEditProfilPage', () => {
  let component: EditEditProfilPage;
  let fixture: ComponentFixture<EditEditProfilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEditProfilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditEditProfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
