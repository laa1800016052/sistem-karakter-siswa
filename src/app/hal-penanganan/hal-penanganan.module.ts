import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HalPenangananPageRoutingModule } from './hal-penanganan-routing.module';

import { HalPenangananPage } from './hal-penanganan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HalPenangananPageRoutingModule
  ],
  declarations: [HalPenangananPage]
})
export class HalPenangananPageModule {}
