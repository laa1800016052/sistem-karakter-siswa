import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { PenangananPage } from '../penanganan/penanganan.page';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-hal-penanganan',
  templateUrl: './hal-penanganan.page.html',
  styleUrls: ['./hal-penanganan.page.scss'],
})
export class HalPenangananPage implements OnInit {
  Dataform: any = {};
  data: any = {};
  userData: any = {};
  form1: any = [];
  id : any;

  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    private navCtrl: NavController,
    public modalController: ModalController,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id=this.route.snapshot.paramMap.get('id')
    this.getForm(this.id);

    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })

    this.db.collection('form').snapshotChanges().subscribe(serverForm => {
      this.form1 = [];
      serverForm.forEach(a => {
        let frm:any = a.payload.doc.data();
        frm.id = a.payload.doc.id;
        this.form1.push(frm);
      })
    })

  }
  selectedSegments: any='hal-penanganan';

  loading:boolean;
  
  getForm(id: any){
    this.db.collection('form').doc(id).get().subscribe(res => {
      this.form1 = res.data();
      console.log(res.data())
    })
  }

  editJadwal(id: any, data:String){
    this.db.collection('form').doc(this.id).update(this.Dataform).then(() => {
      alert('Berhasil mengedit data...!')
    }).catch(function(error) {
      console.error("Error, Gagal Edit Data...!?", error);
    });
    this.router.navigate(['/penanganan'])
  }
  async penanganan(){
    const modal = await this.modalController.create({
      component: PenangananPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
} 
