import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormPenangananPageRoutingModule } from './form-penanganan-routing.module';

import { FormPenangananPage } from './form-penanganan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormPenangananPageRoutingModule
  ],
  declarations: [FormPenangananPage]
})
export class FormPenangananPageModule {}
