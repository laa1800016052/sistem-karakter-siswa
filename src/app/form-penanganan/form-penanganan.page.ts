import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { PenangananPage } from '../penanganan/penanganan.page';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-form-penanganan',
  templateUrl: './form-penanganan.page.html',
  styleUrls: ['./form-penanganan.page.scss'],
})
export class FormPenangananPage implements OnInit {
  data: any = {};
  userData: any = {};

  constructor(
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public router: Router,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })
  }

  loading:boolean;

  simpan(){
    this.loading = true;
    this.data.author = this.userData.email;
    var id = new Date().getTime().toString();
    this.db.collection('form').doc(id).set(this.data).then(res=>{
      this.loading= false;
      alert('Data penanganan siswa berhasil disimpan');
      this.penanganan();
    }).catch(err=>{
      this.loading = false;
      alert('Tidak dapat menyimpan data');
    })
  }
async penanganan() {
  const modal = await this.modalController.create({
    component: PenangananPage,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}
}
