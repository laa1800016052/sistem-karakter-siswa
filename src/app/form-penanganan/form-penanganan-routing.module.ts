import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormPenangananPage } from './form-penanganan.page';

const routes: Routes = [
  {
    path: '',
    component: FormPenangananPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormPenangananPageRoutingModule {}
