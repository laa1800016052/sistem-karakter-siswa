// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAgdBjgR7xl3wv0Wa7lT-XDVY8YzjKU_Gk",
    authDomain: "authsikasi-af2e8.firebaseapp.com",
    projectId: "authsikasi-af2e8",
    storageBucket: "authsikasi-af2e8.appspot.com",
    messagingSenderId: "907246710705",
    appId: "1:907246710705:web:d2b5a4c10321543c8ad790"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
